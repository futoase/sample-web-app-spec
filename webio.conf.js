exports.config = {
  specs: [
    __dirname + '/dest/specs/*.spec.js'
  ],
  capabilities: [{
    browserName: 'chrome'
  }],
  logLevel: 'verbose',
  coloredLogs: true,
  screenshotPath: './screenshots/',
  baseUrl: 'http://localhost:8080/',
  waitforTimeout: 100000,
  connectionRetryTimeout: 100000,
  connectionRetryCount: 3,
  framework: 'mocha',
  reporters: ['dot'],
  mochaOpts: {
    ui: 'bdd',
    timeout: 30000
  }
}
