'use strict';

import Page from './page';

class CommentsPage extends Page {
  get username() {
    return browser.element('input[name=user_name]');
  }

  get comment() {
    return browser.element('input[name=user_comment]');
  }

  get form() {
    return browser.element('form');
  }

  open() {
    super.open();
  }

  submit() {
    this.form.submitForm();
  }
}

module.exports = new CommentsPage();
