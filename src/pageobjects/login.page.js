'use strict';

import Page from './page';

class LoginPage extends Page {
  get username() {
    return browser.element('input[name=login_user_name]');
  }

  get password() {
    return browser.element('input[name=login_password]');
  }

  get form() {
    return browser.element('form');
  }

  open() {
    super.open();
  }

  submit() {
    this.form.submitForm();
  }
}

module.exports = new LoginPage();
