'use strict';

import Page from './page';

class LogoutPage extends Page {
  logout() {
    browser.click('a[href="/logout"]');
  }

  open() {
    super.open();
  }
}

module.exports = new LogoutPage();
