import chai from 'chai';
import LoginPage from './../pageobjects/login.page';
import LogoutPage from './../pageobjects/logout.page';
var expect = chai.expect;
var date = new Date();

describe('logout', function() {
  beforeEach(function() {
    LoginPage.open();
    LoginPage.username.setValue('user');
    LoginPage.password.setValue('password');
    LoginPage.submit();
  });

  it('should logout', function() {
    LogoutPage.open();
    LogoutPage.logout();

    expect(browser.getText('h1')).to.contain('Login');
  });
});
