import chai from 'chai';
import LoginPage from './../pageobjects/login.page';
var expect = chai.expect;
var date = new Date();

describe('login - comment - logout', function() {
  it('should allow access', function() {
    LoginPage.open();
    LoginPage.username.setValue('user');
    LoginPage.password.setValue('password');
    LoginPage.submit();

    expect(browser.getText('h1')).to.contain('Comments');
  });
});
