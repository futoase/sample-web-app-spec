import chai from 'chai';
import LoginPage from './../pageobjects/login.page';
import CommentPage from './../pageobjects/comment.page';
var expect = chai.expect;
var date = new Date();

describe('comments', function() {
  var currentDateTime = date.getTime();

  beforeEach(function() {
    LoginPage.open();
    LoginPage.username.setValue('user');
    LoginPage.password.setValue('password');
    LoginPage.submit();
  });

  it('should be post comments', function() {
    CommentPage.open();
    CommentPage.username.setValue('test user ' + currentDateTime);
    CommentPage.comment.setValue('test comment ' + currentDateTime);
    CommentPage.submit();

    expect(browser.getText('.row*=test comment ' + currentDateTime)).to.contain('test');
  });
});
