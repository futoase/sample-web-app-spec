# sample-web-app-spec

required: https://bitbucket.org/futoase/sample-web-app-for-webdriver.io

# setup

```
> npm install
```

# execute

```
> ./node_modules/.bin/wdio webio.conf.js
```

# LICENSE

Apache 2.0
