var webdriverio = require('webdriverio');
var options = {
  desiredCapabilities: {
    browserName: 'chrome'
  }
};

webdriverio
  .remote(options)
  .init()
  .url('http://localhost:8080/')
  .getTitle().then((title) => {
    console.log('Title was: ' + title);
  })
  .end();
