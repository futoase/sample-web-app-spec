'use strict';

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _login = require('./../pageobjects/login.page');

var _login2 = _interopRequireDefault(_login);

var _comment = require('./../pageobjects/comment.page');

var _comment2 = _interopRequireDefault(_comment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expect = _chai2.default.expect;
var date = new Date();

describe('comments', function () {
  var currentDateTime = date.getTime();

  beforeEach(function () {
    _login2.default.open();
    _login2.default.username.setValue('user');
    _login2.default.password.setValue('password');
    _login2.default.submit();
  });

  it('should be post comments', function () {
    _comment2.default.open();
    _comment2.default.username.setValue('test user ' + currentDateTime);
    _comment2.default.comment.setValue('test comment ' + currentDateTime);
    _comment2.default.submit();

    expect(browser.getText('.row*=test comment ' + currentDateTime)).to.contain('test');
  });
});