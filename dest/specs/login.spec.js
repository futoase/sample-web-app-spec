'use strict';

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _login = require('./../pageobjects/login.page');

var _login2 = _interopRequireDefault(_login);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expect = _chai2.default.expect;
var date = new Date();

describe('login - comment - logout', function () {
  it('should allow access', function () {
    _login2.default.open();
    _login2.default.username.setValue('user');
    _login2.default.password.setValue('password');
    _login2.default.submit();

    expect(browser.getText('h1')).to.contain('Comments');
  });
});