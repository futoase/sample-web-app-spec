'use strict';

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _login = require('./../pageobjects/login.page');

var _login2 = _interopRequireDefault(_login);

var _logout = require('./../pageobjects/logout.page');

var _logout2 = _interopRequireDefault(_logout);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expect = _chai2.default.expect;
var date = new Date();

describe('logout', function () {
  beforeEach(function () {
    _login2.default.open();
    _login2.default.username.setValue('user');
    _login2.default.password.setValue('password');
    _login2.default.submit();
  });

  it('should logout', function () {
    _logout2.default.open();
    _logout2.default.logout();

    expect(browser.getText('h1')).to.contain('Login');
  });
});